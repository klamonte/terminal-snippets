How To Do Things In ncurses And VT100/Xterm Terminals
=====================================================

This document is intended to be a "FAQ-lite" for medium to advanced
ncurses programming and Xterm-like terminal emulation questions.  It
is assumed that the reader already knows how to get a basic ncurses
"hello world" running.  This guide is intended to help programmers get
from "hello world" to complete control of keyboard, screen, and mouse
needed to support advanced text user interfaces.

This is an opinionated list.  What can I say, I actually like this
stuff.  I have been programming a ncurses-based terminal that
impersonates other terminals for 15+ years, and made a lot of
mistakes.


Keyboard: How Do I Read Ctrl-Z?
-------------------------------

Two solutions:

* Leave the terminal in normal (cooked, noraw()) mode and assume that
  KEY_SUSPEND is Ctrl-Z.

* Put the terminal in raw() mode and read it as ASCII 0x1A (SUB).


Keyboard: How Do I Read Ctrl-C?
-------------------------------

* Put the terminal in raw() mode and read it as ASCII 0x03 (ETX).


Keyboard: How Do I Read Alt-{something}?
----------------------------------------

Follow this procedure:

1. Xterm needs to have its metaSendsEscape resource set so that it
   sends ESCAPE (ASCII 0x1B) first.  Either put
   'XTerm*metaSendsEscape: true' in ~/.Xresources, or send
   "\033[?1036h" to it.

2. OPTIONAL: If you want the user to be able to hit ESCAPE as a
   separate key without a long delay, then before calling initscr() or
   newterm(), call 'putenv("ESCDELAY=20");' to reduce ncurses' ESCAPE
   timeout to 20 milliseconds.  20 is arbitrary, and might need to be
   larger for your environment.  If it is too short then you will see
   it when wget_wch() returns a series of distinct characters such as
   'ESC [ A' rather than KEY_UP.

3. Call wget_wch(). If it returns OK and has ESC (0x1B) in the wint_t,
   then set nodelay(TRUE), and call wget_wch() again:

   a. If it returns ERR then this was a bare ESCAPE.

   b. If it does not return ERR, then this was either ALT-{something}
      or the beginning of an extended keystroke that is not in the
      terminfo entry for that terminal.  You can choose to continue
      reading until you have a full sequence and translate that to a
      function key, but you are ASSUMING that you know what the
      terminal really is.

   c. Don't forget to call nodelay(FALSE) again if you had had
      blocking keyboard input before reading the original ESCAPE.


Keyboard: Why Should I Only Use wget_wch() And Never Use getch()?
-----------------------------------------------------------------

We live in a Unicode world now.  Only use wget_wch().

If you have only one ncurses window, then call wget_wch(stdscr, ...).


Keyboard: How Do I Read Extended Function Keys From Xterm?
----------------------------------------------------------

Initialize ncurses similarly to this:

```
use_extended_names(TRUE);
initscr();
raw();
nodelay(stdscr, TRUE);
noecho();
nonl();
intrflush(stdscr, FALSE);
meta(stdscr, TRUE);
keypad(stdscr, TRUE);
```

Read a keystroke like this:

```
wint_t utf_keystroke;
int res;

res = wget_wch(window, &utf_keystroke);
/* res will be either ERR (no data), KEY_CODE_YES, or OK. */

if (res == ERR) {
    /* non-blocking input, and nothing was here. */

} else if (res == KEY_CODE_YES) {

    /* Mouse, screen resize, or function/arrow key */
    if (utf_keystroke == KEY_MOUSE) {
        /* Call a function to process the mouse... */
    } else if (utf_keystroke == KEY_RESIZE) {
        /* Call a function to get new screen size information... */
    } else if (utf_keystroke == KEY_SUSPEND) {
        /* This was probably Ctrl-Z */
    } else if ((utf_keystroke < KEY_MIN) || (utf_keystroke > KEY_MAX)) {
        /*
         * This is an extended keycode.  It does not have an integer
         * constant to compare to, but it does have a name.
         *
         * Adding entries to terminfo and checking for those names
         * here is the future-proofed supported method to detect new
         * keystrokes with ncurses.  One could easily add multimedia
         * keys, shift/ctrl/alt/super modifiers, or more.
         */
        if (strcmp(keyname(utf8_keystroke), "kNXT6") == 0) {
            /* This is Ctrl-Shift-Down, handle it */
        } else if (strcmp(keyname(utf8_keystroke), "kDC4") == 0) {
            /* This is Alt-Shift-Delete, handle it */
        } else if (...) {
            /*
             * ...and so on.  Probably want a state table here, but
             * really up to you.
             */
        }
    }

} else if (res == OK) {
    /* this is a normal Unicode character not-function-key */
    if (utf8_keystroke == 0x1B) {
        /*
         * This is ESC.  It might be the beginning of a keyboard
         * sequence that isn't in terminfo, or it might be
         * Alt+{something}.  See the Alt-{something} question to pick
         * up from here.
         */
    } else {
        /*
         * This is a Unicode code point, which could also be a control
         * character.
         */
    }
}
```


Keyboard: How Do I Get The Sequence Of A Key For A Different Terminal Type?
---------------------------------------------------------------------------

Suppose your TERM is "xterm" where F1 is 'ESC O P', but you would
really like to see what terminfo thinks the F1 key would be for
"linux" where F1 is 'ESC [ [ A'.  Perhaps you are writing a terminal
emulator that will impersonate "linux" to another system, who knows?

You can get your answer via newterm().  But there is a small trick:
you need to initialize curses with TERM for stdscr, then interrogate
it with code for "linux", then switch back to stdscr.  The code
overall looks like this:

```
SCREEN * stdscr_screen = newterm(getenv("TERM"), stdout, stdin);
set_term(stdscr_screen);

FILE * dev_null = fopen("/dev/null");
SCREEN * fake_screen = newterm("linux", dev_null, dev_null);
if (fake_screen != NULL) {
    set_term(fake_screen);

    char * linux_f1 = tigetstr("kf1");
    /*
     * linux_f1 now has the sequence one would see when a Linux
     * console user presses F1.
     */

    endwin();
    delscreen(fake_screen);
}
fclose(dev_null);
set_term(stdscr_screen);
```

This trick does not work everywhere, some systems used to crash when
using multiple calls to newterm() and delscreen() due a ncurses bug
which was fixed circa 2011.


Screen: How Do I Output Unicode?
--------------------------------

Use setcchar(), mvwadd_wch(), and wadd_wch().  For lines, use
mvwhline_set() and mvwvline_set().  Example code:

```
    cchar_t ncurses_ch;
    wchar_t wch[2];
    wch[0] = ch;
    wch[1] = 0;
    setcchar(&ncurses_ch, wch, A_NORMAL, 0, NULL);
    mvwadd_wch(stdscr, y, x, &ncurses_ch);
```

This example just spits one code point to screen.  It is possible to
add more code points for the same cell (e.g. accents), just make wch[]
bigger and put more code points in the array.


Screen: How Do I Output 24-bit RGB With ncurses?
------------------------------------------------

You don't, because ncurses lacks 24-bit color support and is unlikely
to ever get it.

Recent branches of PDcurses support 64-bit chtype with 30 bits
reserved for color: 15 bits foreground, 15 bits background.  See the
A_RGB macro in PDcurses.


Screen: How Do I Output 24-bit RGB To Xterm?
--------------------------------------------

For a foreground color change:

```
int colorRed     = (colorRGB >> 16) & 0xFF;
int colorGreen   = (colorRGB >>  8) & 0xFF;
int colorBlue    =  colorRGB        & 0xFF;
printf(stdout, "\033[38;2;%d;%d;%dm", colorRed, colorGreen, colorBlue);
```

For background color, change the "38" into "48":

```
int colorRed     = (colorRGB >> 16) & 0xFF;
int colorGreen   = (colorRGB >>  8) & 0xFF;
int colorBlue    =  colorRGB        & 0xFF;
printf(stdout, "\033[48;2;%d;%d;%dm", colorRed, colorGreen, colorBlue);
```


Screen: Why Is 24-bit RGB Making It Have Bad Color And Sometimes Blink?
-----------------------------------------------------------------------

The terminal you are using followed the ANSI X3.64 / ECMA-48 standards
correctly, saw the RGB code as a series of distinct stateless SGR
codes, and did the right thing which is look like shit, because the
24-bit RGB color "standard" part of ITU T.416 (13.1.8) was shit to
begin with.

There are two commonly used kinds of colors, RGB and indexed, and the
sequence to select indexed color uses '5', which to a compliant
VT100-type terminal means blink.  Unfortunately, everyone assumes that
supporting RGB color also means supporting indexed color, so
applications blindly use both, hence the blinking.

24-bit RGB has been gaining support, but it is a broken hack inside an
otherwise consistent escape sequence parser, and it looks terrible on
real terminal hardware.  Fortunately it appears that this is about the
last broken hack this generation will be adding to VT100/Xterm type
terminals.


Screen: I Want To Make A New Escape Sequence To ...
---------------------------------------------------

No you don't.  Seriously, DON'T.

Don't create the need for another brokenLinuxOSC Xterm resource, or
recreate the BBS-era "ANSI Music" sequence that sets the VT100 special
graphics character set and hoses people using not-braindead-ANSI.SYS
terminals.

Trust me, you have a solution already here.  The Ancient Order Of Text
Video Terminal Designers knew that this day would come, and someone
would need to convey data to a custom terminal without hosing the
screen.  They also knew that if they provided a failsafe method to
convey data to a custom terminal that would not screw up life for the
other existing terminals, people would not think to look for it.  So
they hedged their bet and provided TWO ways to send custom data to a
terminal that won't screw up life for the other existing terminals.

These two methods are the Application Program Command (APC) and the
Privacy Message (PM).  Both of these modes are entered with a sequence
and exited with ST ('ESC \\' or 0x9C).  Whatever occurs between the
APC/PM start and ST can be ANYTHING, and it is completely ignored by
the terminal.

You want to download a new font on-the-fly to the terminal?  'ESC _
"setFont:" {font data in base64} ESC \\' Bam, done, and no garbage
displayed on the screen of users of putty, Linux console, or other
terminals.  Want to splat a bitmap on screen?  'ESC _ "bitmap:"
{bitmap data in base64} ESC \\' Done!  Hell, you could stick a whole
HTTP response in between the PM and ST sequences and no one would
care.

Now you know, and knowing is half the battle.  Build your cool new
stuff with APC or PM and go in peace.


Screen: How Do I Determine If A Terminal Supports My New Extension?
-------------------------------------------------------------------

You are in luck, there are FOUR future-proof methods.  Send any of the
following sequences to the terminal, and look at its response:

* 'ESC Z' - DECID.  The terminal will respond with
  'ESC [ ? {something} c' .  See if {something} is the special
  sequence of digits and semicolons that identifies your terminal.

* 'ESC [ c' - DA.  The terminal will respond just like DECID above.

* 'ESC [ ? {X} n' - DSR.  Use a custom {X} that is not: 5, 6, 15, 25,
  26, 27, 50, or 53.  Perhaps {X} of 1000-2000 would work for you.
  The terminal will respond with 'ESC [ ? {something} n', where
  {something} is the special sequence of digits and semicolons that
  identifies your terminal.

* 'ENQ' (ASCII 0x05, Enquiry).  ENQ was a method to identify the
  remote terminal that is still supported on real hardware (usually in
  a settings menu) and under Xterm via the answerbackString resource.
  Users might set the answer back as "Terminal X in room Y" for
  example.  The default answer back is nothing, but you can make your
  terminal respond with something specific like "CoolAwesomeTermV34.6"
  or something.

Please do not invent a new way to determine terminal characteristics.
RIPscrip tried that with their 'ESC [ !'  sequences in 1992-1993 and
anyone who had a good terminal ran the risk of seeing RIPscrip's
"detection" turn into soft terminal reset DECSTR ('ESC [ ! p').


Screen: How Do I Output Double-Width / Double-Height Characters?
----------------------------------------------------------------

This is not possible with only the ncurses API.

However, VT100 and Xterm support double-width and double-height LINES
(not single characters), and they can be changed both before and after
being written to.

If you are very careful, you can send the double-width/double-height
sequences and flush output with fflush(stdout) to tell the terminal to
change the line(s), and then draw a line with ncurses and flush output
with refresh() to get the Unicode glyphs on screen, and you will end
up with a double-width and/or double-height line on the screen.  You
will have to maintain your own state of what the visible line is
supposed to be (single vs double), and know when to tell the terminal
to UNSET the double flag.

See LCXterm's render_scrollback() in scrollback.c for an example of
this.


Mouse: How Do I Get Mouse Motion Events With ncurses?
-----------------------------------------------------

ncurses is pretty straightforward with mouse clicks, but mouse motion
and drag-and-drop are slightly harder.  You need to set your TERM to
xterm-1002, xterm-1003, xterm-1005, or xterm-1006 before calling
initscr() or newterm() for ncurses to report full mouse motion events.

Note that if you want DOS-style mouse support where you know at any
given moment which buttons are down and whether or not it was a
drag/motion event, you will need to maintain your own state knowledge
of the buttons.  LCXterm's handle_mouse() in input.c has logic for
this.


Mouse: How Should I Get Mouse Events With Xterm?
------------------------------------------------

If you are talking to Xterm directly, and not through ncurses, do the
following:

1. Use UTF-8 for everything.

2. Send the Xterm sequences to enable mouse tracking, then UTF-8
   coordinates, then SGR coordinates.  In C that would be:
   'printf("\033[?1002;1003;1005;1006h";)'

3. When the program is exiting, reset the mouse.  Either turn it off
   ('printf("\033[?1002;1003;1005;1006l");') or reset the terminal
   with the RIS sequence ('ESC c').

The reason you want to do it this way is to a) not be limited to (160,
94) or (223, 223) maximum (columns, rows), and b) not corrupt the
UTF-8 stream.  The best mouse protocol is SGR (1006 mode) because it
is unambiguous and straight ASCII, the second best is UTF-8 (1005
mode) because it has a wide range.  But here is the real trick: if you
ask for both, then if the terminal supports EITHER ONE you can
unambiguously parse that and have a full-featured mouse (including
wheel up/down) with a wide range.

If that doesn't work, then you are talking to a very old terminal with
neither UTF-8 or SGR mouse protocol support.  Which sucks because you
can either get garbage "keyboard" input characters when the mouse goes
outside (160, 94) (because those coordinates can look like valid UTF-8
encoded code points) or a corrupted UTF-8 stream.


Other: What Should Terminals Do Next?
-------------------------------------

Honestly, if a terminal just supports the basics of vttest, can
consume 24-bit RGB (parse and discard if nothing else, because people
will not stop sending it), uses UTF-8, and remembers to use APC or PM
for custom new stuff, then 99.9995% of its users will be happy.

The pain points in terminals is not the ANSI X3.64 / ECMA-48 parts, or
even the X10 mouse anymore, those are fine now.  And if one looks at
how poorly putty and the Linux console handle vttest, yet remembers
that people are generally quite happy with them, really it's just
enough to support everything ncurses and tmux will spit out on the
xterm terminfo entry that makes a decent enough grade.

The main innovation in terminals is going to come down from two
places:

1. For the non-Windows world: stuff that happens after glyphs are on
   the screen.  Integration with the desktop environment.  Fast
   scrollback, selectable / clickable hyperlinks, search, transparent
   backgrounds, 3D effects, all of those things.  Everyone has their
   itches, lots of scratching is going on, and lots of neat new cool
   things will come out to energize people.

2. For the Windows world: the next decade will be exciting to see as
   the Windows console catches up to the capabilities of Procomm Plus
   2.x circa 1994.  Resizable windows, a little bit more VT100
   support, some custom colors, and maybe someday even X10 mouse
   support.  Who knows, the sky is the limit!

There is not a lot more to do for the "terminal" part of terminals.
VT100 is old, but it's mature and is not going to go away.

The space for "interactively mixing text and graphics" still has
plenty of room to explore though.  But at this point TeX is dying and
the best platform is probably the web browser stack: Electron-based
applications that "think" in HTML/CSS/Javascript.  I see room for a
whole new paradigm to grow in the "mixing text and graphics" world, a
"Hollywood OS" concept, but it won't be compatible with ncurses or
VT100/Xterm.
